#!/bin/bash

read -p "Enter the domain name:  " domain_name
read -p "Enter the Document Root:  " doc_root
read -p "Enter configration file name:  " conf_file
read -p "Enter the port number:  " port_num
read -p "Enter the content directory:  " content

if [ -z "$domain_name" ] || [ -z "$doc_root" ] || [ -z "$conf_file" ] || [  -z "$port_num" ] || [ -z "$content" ]; then
echo "No sufficient informations"
exit 1
fi


while true; do
echo -n "#"
sleep .5
done &


yum install -y httpd &>/dev/null

mkdir -p $doc_root
mkdir -p ${doc_root}/logs

cat >>  /etc/httpd/conf.d/$conf_file <<EOF

<VirtualHost *:${port_num}>
	ServerName $domain_name
	DocumentRoot $doc_root
	CustomLog "${doc_root}/logs/access_log" common
	ErrorLog "${doc_root}/logs/error_log"
</VirtualHost>

<Directory "$doc_root">
	Options Indexes FollowSymLinks
	Require all granted
</Directory> 

EOF

semanage fcontext -a -t httpd_sys_content_t "${doc_root}(/.*)?"
restorecon -RvF $doc_root &>/dev/null
semanage fcontext -a -t httpd_log_t "${doc_root}/logs(/.*)?"
restorecon -RvF ${doc_root}/logs &>/dev/null
chcon -R -t httpd_sys_content_t ${content}

firewall-cmd --add-port=${port_num}/tcp --permanent >/dev/null
firewall-cmd --reload >/dev/null

ln -s ${content}/index.* $doc_root/ &>/dev/null

systemctl restart httpd
systemctl enable httpd &>/dev/null



kill $! ; trap 'kill $!' SIGTERM
exit 0 ;;

